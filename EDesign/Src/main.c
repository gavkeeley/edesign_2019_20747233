/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 *
 ******************************************************************************
 * Title: LIS2DH12 DS driver
 * Author: STMicroelectronics
 * Date: 15/05/2018
 * Revision: 6.0
 * Avaliabilty: "https://github.com/STMicroelectronics/
 * 					STMems_Standard_C_drivers/tree/master/lis2dh12_STdC"
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "lis2dh12_reg.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define RXSIZE    200
#define RS    GPIO_PIN_9
#define RNW    GPIO_PIN_7
#define E    GPIO_PIN_9
#define DB4    GPIO_PIN_8
#define DB5    GPIO_PIN_4
#define DB6    GPIO_PIN_5
#define DB7    GPIO_PIN_10

#define SENSOR_BUS hi2c1 //i2c
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
ADC_HandleTypeDef hadc2;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint8_t receiveBuffer[1];
extern volatile uint8_t timerTick;
volatile uint8_t receivedData;
int count = 0;

//--------GPS Variables--------

double lat = 0.0;
double lon = 0.0;
double alt = 0.0;
char timeOfDay[] = "00:00:00";

uint8_t received[RXSIZE];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM2_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_ADC2_Init(void);
static void MX_ADC1_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
static int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);
static int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);
void writeToLCD(int altitude, int burnSig);
void delay(int length);
void writeLCDString(char str[16]);
void writeLCDCmd(char data);
void writeLCDData(char data);
void setLCDDataPins(uint8_t data, uint8_t highNibble);
void setLCDCtrlPins(uint8_t RSState, uint8_t RNWState, uint8_t EState);

static axis3bit16_t data_raw_acceleration;
static axis1bit16_t data_raw_temperature;
static float acceleration_mg[3];
static float temperature_degC;
static uint8_t whoamI;
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_USART2_UART_Init();
	MX_TIM2_Init();
	MX_USART1_UART_Init();
	__HAL_TIM_ENABLE(&htim2);
	__HAL_TIM_ENABLE_IT(&htim2, TIM_IT_UPDATE);
	MX_ADC2_Init();
	MX_ADC1_Init();
	MX_I2C1_Init();
	/* USER CODE BEGIN 2 */

	char* stdNo = "20747233";

	double lon = 0.0, lat = 0.0;
	double alt = 0.0;

	int time = -1;				//so that first UART message is at time 1
	int cs = 0;
	int outOfBoundsCount = 0;
	int outOfBoundsTime = -1;
	uint8_t outOfBounds = 0;

	HAL_Delay(10);
	writeLCDCmd(0b00000010);	//cursor to first position
	writeLCDCmd(0b00101000);	//4-bit mode, 2 'lines' of 8 chars
	writeLCDCmd(0b00001100);	//cursor off and non-blinking

	lis2dh12_ctx_t dev_ctx;

	dev_ctx.write_reg = platform_write;
	dev_ctx.read_reg = platform_read;
	dev_ctx.handle = &hi2c1;

	lis2dh12_device_id_get(&dev_ctx, &whoamI);
	if (whoamI != LIS2DH12_ID)
	{
		while(1)
		{
		  //LIS2DH12 Not found
		}
	}

	lis2dh12_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);
	lis2dh12_data_rate_set(&dev_ctx, LIS2DH12_ODR_1Hz);
	lis2dh12_full_scale_set(&dev_ctx, LIS2DH12_2g);
	lis2dh12_temperature_meas_set(&dev_ctx, LIS2DH12_TEMP_ENABLE);
	lis2dh12_operating_mode_set(&dev_ctx, LIS2DH12_HR_12bit);

	float volts = 0.0;
	int current = 0;

	int currentADC = 0;
	int currCount = 0;

	int accX = 0;
	int accY = 0;
	int accZ = 0;
	int accCount = 0;

	int sys = 0;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1)
	{

		sys++;
		if(sys%10000 == 0)
		{

			HAL_ADC_Start(&hadc2);
			HAL_ADC_PollForConversion(&hadc2, 1000);
			currentADC += HAL_ADC_GetValue(&hadc2);
			HAL_ADC_Stop(&hadc2);
			currCount++;
		}

		if(sys%10000 == 0)
		{
			lis2dh12_reg_t reg;
			lis2dh12_xl_data_ready_get(&dev_ctx, &reg.byte);
			if (reg.byte)
			{
				/* Read accelerometer data */
				memset(data_raw_acceleration.u8bit, 0x00, 3*sizeof(int16_t));
				lis2dh12_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit);
				acceleration_mg[0] = lis2dh12_from_fs2_hr_to_mg(data_raw_acceleration.i16bit[0]);
				acceleration_mg[1] = lis2dh12_from_fs2_hr_to_mg(data_raw_acceleration.i16bit[1]);
				acceleration_mg[2] = lis2dh12_from_fs2_hr_to_mg(data_raw_acceleration.i16bit[2]);

				accX += acceleration_mg[2];
				accY += acceleration_mg[1];
				accZ += acceleration_mg[0];
				accCount++;
			}
			lis2dh12_temp_data_ready_get(&dev_ctx, &reg.byte);
			if (reg.byte)
			{
			  /* Read temperature data */
			  memset(data_raw_temperature.u8bit, 0x00, sizeof(int16_t));
			  lis2dh12_temperature_raw_get(&dev_ctx, data_raw_temperature.u8bit);
			  temperature_degC =
				lis2dh12_from_lsb_hr_to_celsius(data_raw_temperature.i16bit);
			}

		}
		if (HAL_UART_Receive_IT(&huart1, (uint8_t*) receiveBuffer, 1) != HAL_OK)
		{
			//if there's a error in the process of receiving UART data run this code
		}

		if (receivedData)
		{
			//*'s for demo 2
			//$type,*time,*lat,N,*lon,E,quality,#sats,hdop,*alt,a-units,undulation,u-units,age,stn ID,*xx[CR][LF]
			received[count++] = receiveBuffer[0];
			if (receiveBuffer[0] == (uint8_t) '\n')	//Checks that the transfer is now complete \n == 10
			{
				if (received[1] == 'G' && received[2] == 'P'
						&& received[3] == 'G' && received[4] == 'G'
						&& received[5] == 'A')//checks that the NMEA starts with GPGGA
				{
					int i;
					int checkSum = 0;
					cs = 0;
					for (i = 1; i < count; i++)		//i = 1 to exclude '$'
					{
						if (received[i] == (uint8_t) '*' && i <= count - 2)
						{
							checkSum +=
									((uint32_t) received[i + 1] <= '9') ?
											((uint32_t) received[i + 1] - '0')
													* 16 :
											((uint32_t) received[i + 1] - 'A'
													+ 10) * 16;
							checkSum +=
									((uint32_t) received[i + 2] <= '9') ?
											((uint32_t) received[i + 2] - '0') :
											((uint32_t) received[i + 2] - 'A'
													+ 10);
							i += 2;
						}
						else if (received[i] != 10 && received[i] != 13)
						{
							cs ^= (uint32_t) received[i];	//XOR the next char
						}

					}
					if (checkSum == cs)
					{
						uint8_t commaCount = 0;
						int i = 0;

						char timeChar[11] = "+0000000000";
						char lattitude[11] = "+0000000000";
						char longitude[11] = "+0000000000";
						char altitude[11] = "+0000000000";

						int latSign = 0;
						int lonSign = 0;

						for (i = 0; i < count; i++)
						{
							int j = 0;
							for (j = 0; j < 11; j++)
							{
								if (received[i + j] != (uint8_t) ',')
								{
									if (commaCount == 1)
										timeChar[j] = received[i + j];
									if (commaCount == 2)
										lattitude[j] = received[i + j];
									if (commaCount == 3)
										latSign =
												(received[i + j] == 'N' ? 1 : -1);
									if (commaCount == 4)
										longitude[j] = received[i + j];
									if (commaCount == 5)
										lonSign =
												(received[i + j] == 'E' ? 1 : -1);
									if (commaCount == 9)
										altitude[j] = received[i + j];
								}
								else
								{
									commaCount++;
									i += j;
									j = 11;

								}
							}
						}
						if (longitude[0] != '+' && lattitude[0] != '+'
								&& altitude[0] != '+' && latSign != 0
								&& lonSign != 0)							//only process data if data has been read in
						{
							lat = latSign
									* ((int) (atof(lattitude) / 100)
											+ fmodl(atof(lattitude), 100.0)
													/ 60.0);
							lon = lonSign
									* ((int) (atof(longitude) / 100)
											+ fmodl(atof(longitude), 100.0)
													/ 60.0);
							alt = atof(altitude);

							timeOfDay[0] = timeChar[0];
							timeOfDay[1] = timeChar[1];
							timeOfDay[2] = ':';
							timeOfDay[3] = timeChar[2];
							timeOfDay[4] = timeChar[3];
							timeOfDay[5] = ':';
							timeOfDay[6] = timeChar[4];
							timeOfDay[7] = timeChar[5];

							if ((lon > 18.9354 || lon < 17.976343)
									&& alt > 10000)
							{
								outOfBoundsCount++;
								if (outOfBoundsCount == 5)
								{
									outOfBoundsTime = HAL_GetTick();
									HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, 1);
									outOfBounds = 1;
								}
							}
							else
							{
								outOfBoundsCount = 0;
							}
						}
						else
						{
							outOfBoundsCount = 0;
						}
					}
				}
				count = 0;
			}
			receivedData = 0;	//end of transfer code
		}
		if (HAL_GetTick() - outOfBoundsTime >= 10000 && outOfBoundsTime > -1)
		{
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, 0);
			outOfBoundsCount = 0;
			outOfBoundsTime = -1;
			outOfBounds = 0;
		}

		if (timerTick && time > -1)
		{
			current = 0.1459627*(currentADC/currCount)+1.642857;
			currCount = 0;
			currentADC = 0;

			uint16_t voltageADC = 0;
			HAL_ADC_Start(&hadc1);
			HAL_ADC_PollForConversion(&hadc1, 1000);
			voltageADC = HAL_ADC_GetValue(&hadc1);
			HAL_ADC_Stop(&hadc1);
			volts = 0.00383051 * voltageADC - 1.04542;

			char transmitBuffer[91];

			int xc = -(int)(accX/accCount) + 30;
			int yc = (int)(accY/accCount) + 45;
			int zc = -(int)(accZ/accCount) - 4;

			accX = 0;
			accY = 0;
			accZ = 0;
			accCount = 0;

			int x = (xc>999?999:(xc<-999?-999:xc));
			int y = (yc>999?999:(yc<-999?-999:yc));
			int z = (zc>999?999:(zc<-999?-999:zc));

			sprintf(transmitBuffer,
					"$%8.8s,%5d,%8s,%3d,%3d,%3d,%4d,%4d,%4d,%+10f,%+11f,%7.1f,%3d,%3.1f\n",
					(uint8_t*) stdNo, (time == 99999 ? time = 0 : ++time),
					(uint8_t*) timeOfDay, (int)temperature_degC, 0, 0, x, y, z, lat, lon, alt,
					current, round(volts * 100) / 100.0);
			HAL_UART_Transmit_IT(&huart1, (uint8_t*) transmitBuffer, 91);
			writeToLCD(alt, outOfBounds);
			timerTick = 0;
		}
		else if (time == -1)
		{
			time++;
			timerTick = 0;
		}
	}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */

/*
 * @brief  Write generic device register (platform dependent)
 *
 * @param  handle    customizable argument. In this examples is used in
 *                   order to select the correct sensor bus handler.
 * @param  reg       register to write
 * @param  bufp      pointer to data to write in register reg
 * @param  len       number of consecutive register to write
 *
 */
static int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp,
                              uint16_t len)
{
  if (handle == &hi2c1)
  {
    /* Write multiple command */
    reg |= 0x80;
    HAL_I2C_Mem_Write(handle, LIS2DH12_I2C_ADD_H, reg,
                      I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
  }
  return 0;
}

/*
 * @brief  Read generic device register (platform dependent)
 *
 * @param  handle    customizable argument. In this examples is used in
 *                   order to select the correct sensor bus handler.
 * @param  reg       register to read
 * @param  bufp      pointer to buffer that store the data read
 * @param  len       number of consecutive register to read
 *
 */
static int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp,
                             uint16_t len)
{
  if (handle == &hi2c1)
  {
    /* Read multiple command */
    reg |= 0x80;
    HAL_I2C_Mem_Read(handle, LIS2DH12_I2C_ADD_H, reg,
                     I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
  }
  return 0;
}

void writeToLCD(int altitude, int burnSig)
{
    char lcdData[16] = "                ";
    int a = (int)altitude;
    int index = 0;
    if (altitude >= 10000)
        lcdData[index++] = (a % 100000 - a % 10000) / 10000 + 48;
    if (altitude >= 1000)
        lcdData[index++] = (a % 10000 - a % 1000) / 1000 + 48;
    if (altitude >= 100)
        lcdData[index++] = (a % 1000 - a % 100) / 100 + 48;
    if (altitude >= 10)
        lcdData[index++] = (a % 100 - a % 10) / 10 + 48;
    lcdData[index++] = (a % 10) + 48;
    lcdData[index++] = 'm';
    lcdData[9] = (burnSig > 0 ? 'B' : ' ');
    lcdData[13] = ((int)(temperature_degC/10)>0?(int)(temperature_degC/10)+'0':' ');
    lcdData[14] = (int)(temperature_degC-(int)(temperature_degC/10)*10)+'0';
    lcdData[15] = 'C';
    writeLCDString(lcdData);
}

void delay(int length)
{
    int i;
    for (i = 0; i < length; i++)
        ;
}

void writeLCDString(char str[16])
{
    writeLCDCmd(0b10000000);    //move cursor to the first char position
    int i;
    for (i = 0; i < 16; i++)
    {
        if (i == 8)
            writeLCDCmd(0b11000000);    //move to the second 'line'
        writeLCDData(str[i]);
    }
}

void writeLCDCmd(char data)
{
    setLCDDataPins(data, 1);    //set pins to the values of the higher nibble
    setLCDCtrlPins(0, 0, 1);    //select command register for writing
    delay(200);
    setLCDCtrlPins(0, 0, 0);    //end EN HIGH-LOW pulse
    delay(8000);

    setLCDDataPins(data, 0);    //set pins to the values of the lower nibble
    setLCDCtrlPins(0, 0, 1);    //select command register for writing
    delay(200);
    setLCDCtrlPins(0, 0, 0);    //end EN HIGH-LOW pulse
    delay(8000);
}

void writeLCDData(char data)
{
    setLCDDataPins(data, 1);    //set pins to the values of the higher nibble
    setLCDCtrlPins(1, 0, 1);    //select data register for writing
    delay(200);
    setLCDCtrlPins(1, 0, 0);    //end EN HIGH-LOW pulse
    delay(8000);

    setLCDDataPins(data, 0);    //set pins to the values of the higher nibble
    setLCDCtrlPins(1, 0, 1);    //select data register for writing
    delay(200);
    setLCDCtrlPins(1, 0, 0);    //end EN HIGH-LOW pulse
    delay(8000);
}

void setLCDDataPins(uint8_t data, uint8_t highNibble)
{
    uint8_t input = data - ((data >> 4) << 4);
    if (highNibble)
        input = data >> 4;
    HAL_GPIO_WritePin(GPIOA, DB7, input >> 3);
    HAL_GPIO_WritePin(GPIOB, DB6, (input >> 2) - ((input >> 3) * 2));
    HAL_GPIO_WritePin(GPIOB, DB5, (input >> 1) - ((input >> 2) * 2));
    HAL_GPIO_WritePin(GPIOA, DB4, input - ((input >> 1) * 2));
}

void setLCDCtrlPins(uint8_t RSState, uint8_t RNWState, uint8_t EState)
{
    HAL_GPIO_WritePin(GPIOB, RS, RSState);
    HAL_GPIO_WritePin(GPIOC, RNW, RNWState);
    HAL_GPIO_WritePin(GPIOA, E, EState);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart1)
{
    receivedData = 1;
}
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_ADC12;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12PLLCLK_DIV1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_MultiModeTypeDef multimode = {0};
  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the ADC multi-mode 
  */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel 
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief ADC2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC2_Init(void)
{

  /* USER CODE BEGIN ADC2_Init 0 */

  /* USER CODE END ADC2_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC2_Init 1 */

  /* USER CODE END ADC2_Init 1 */
  /** Common config 
  */
  hadc2.Instance = ADC2;
  hadc2.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc2.Init.Resolution = ADC_RESOLUTION_12B;
  hadc2.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc2.Init.ContinuousConvMode = DISABLE;
  hadc2.Init.DiscontinuousConvMode = DISABLE;
  hadc2.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc2.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc2.Init.NbrOfConversion = 1;
  hadc2.Init.DMAContinuousRequests = DISABLE;
  hadc2.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc2.Init.LowPowerAutoWait = DISABLE;
  hadc2.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  if (HAL_ADC_Init(&hadc2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel 
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC2_Init 2 */

  /* USER CODE END ADC2_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 8000;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 7999;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_8|GPIO_PIN_9, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PC7 */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PA8 PA9 PA10 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB4 PB5 PB8 PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while (1)
	{
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	 tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
